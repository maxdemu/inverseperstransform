#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv2/opencv.hpp>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    
public slots:
	void slotButtonSelectInputDir();
	void slotButtonReadAndCalc();
	void slotButtonSelectOutputDir();
	void slotButtonWrite();

private:
    Ui::MainWindow *ui;
    bool _inputSpecified;
    bool _outputSpecified;
    std::string _inputDir;
    std::string _outputDir;
    std::vector<cv::Point3d> _points;
    
};
#endif // MAINWINDOW_H
