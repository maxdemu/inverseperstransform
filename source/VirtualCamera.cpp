#include "VirtualCamera.h"

#include <opencv2/opencv.hpp>
#include <iostream>

//#include "printmat.h"

#define PI 3.14159265

VirtualCamera::VirtualCamera(
	cv::Point3f position,
	cv::Point3f reference,
	cv::Point3f	up,
	float fovX,
	float fovY,
	int pixelWidth,
	int pixelHeight,
	float nearClipPlane
	)
{
	_position = position;
	_reference = reference;
	_up = up;
	_fovX = fovX;
	_fovY = fovY;
	_pixelWidth = pixelWidth;
	_pixelHeight = pixelHeight;
	_nearClipPlane = nearClipPlane;
	
	_halfHeight = ((float)_pixelHeight) / 2.0f;
	_halfWidth = ((float)_pixelWidth) / 2.0f;
	
	//using namespace std;
	
	//cout << "Camera params:" << endl;
	//cout << "Posiiton: " 		<< _position.x << ", " 	<< _position.y << ", " 	<< _position.z << endl;
	//cout << "Reference: " 		<< _reference.x << ", " << _reference.y << ", " << _reference.z << endl;
	//cout << "Up: " 				<< _up.x << ", " 		<< _up.y << ", " 		<< _up.z << endl;
	//cout << "fovx: " 		<< _fovX << endl;
	//cout << "fovy: " 		<< _fovY << endl;
	//cout << "Width: " 		<< _pixelWidth << endl;
	//cout << "Height: " 		<< _pixelHeight << endl;
	//cout << "NearClip: " 	<< _nearClipPlane << endl;
	
	//Calculate world to camera matrix (and inverse)
	cv::Point3f z = _reference - _position;
	float len = cv::norm(z);
	z = z / len;
	
	cv::Point3f x = _up.cross(z);
	
	cv::Point3f y = z.cross(x);
	
	cv::Mat camToWorld(4, 4, CV_32FC1);
	
	camToWorld.at<float>(0,0) = x.x;
	camToWorld.at<float>(1,0) = x.y;
	camToWorld.at<float>(2,0) = x.z;
	camToWorld.at<float>(3,0) = 0.0f;
	
	camToWorld.at<float>(0,1) = y.x;
	camToWorld.at<float>(1,1) = y.y;
	camToWorld.at<float>(2,1) = y.z;
	camToWorld.at<float>(3,1) = 0.0f;
	
	camToWorld.at<float>(0,2) = z.x;
	camToWorld.at<float>(1,2) = z.y;
	camToWorld.at<float>(2,2) = z.z;
	camToWorld.at<float>(3,2) = 0.0f;
	
	camToWorld.at<float>(0,3) = _position.x;
	camToWorld.at<float>(1,3) = _position.y;
	camToWorld.at<float>(2,3) = _position.z;
	camToWorld.at<float>(3,3) = 1.0f;
	
	_camToWorld = camToWorld.clone();
	
	//printMat(_camToWorld, "_camToWorld");
	
	cv::Mat worldToCam(4, 4, CV_32FC1);
	
	_worldToCam = camToWorld.inv();
	
	//printMat(_worldToCam, "_worldToCam");
	
	//Calculate fx and fy
	
	float halfFovX = _fovX / 2.0f;
	float halfFovY = _fovY / 2.0f;
	
	//fov needs to be multiplied by PI and divided by 180 to convert it from deg to rad
	float tanX = (	tan((halfFovX * (float)PI) / 180.0f)	) * 2.0f;
	float tanY = (	tan((halfFovY * (float)PI) / 180.0f)	) * 2.0f;
	
	//std::cout << "tanx: " << tanX << std::endl;
	//std::cout << "tany: " << tanY << std::endl;
	
	float fx = ((float)_pixelWidth) / tanX;
	float fy = ((float)_pixelHeight) / tanY;
	
	_fx = fx;
	_fy = fy;
	
	//std::cout << "_fx: " << _fx << std::endl;
	//std::cout << "_fy: " << _fy << std::endl;
	
}

cv::Point2i VirtualCamera::forwardTransform(cv::Point3f point)
{
	cv::Mat pointHom(4, 1, CV_32FC1);
	pointHom.at<float>(0,0) = point.x;
	pointHom.at<float>(1,0) = point.y;
	pointHom.at<float>(2,0) = point.z;
	pointHom.at<float>(3,0) = 1.0f;
	
	//printMat(pointHom, "pointHom");
	
	cv::Mat pointCam(4, 1, CV_32FC1);
	pointCam = _worldToCam * pointHom;
	//The w-coordinate will always be 1; no need to normalize
	
	//printMat(pointCam, "pointCam");
	
	cv::Mat pointPers(4, 1, CV_32FC1);
	pointPers.at<float>(0,0) = pointCam.at<float>(0,0);
	pointPers.at<float>(1,0) = pointCam.at<float>(1,0);
	pointPers.at<float>(2,0) = pointCam.at<float>(2,0);
	pointPers.at<float>(3,0) = pointCam.at<float>(2,0) * _nearClipPlane;
	
	//printMat(pointPers, "pointPers (prev)");
	
	//Normalize the homogenous w-coordinate
	pointPers = pointPers / pointPers.at<float>(3,0);
	//The z-coordinate of this point will always be equal to _nearClipPlane
	
	//printMat(pointPers, "pointPers");
	
	//Calculate pixelCoords
	
	float pixelX = (_fx * pointPers.at<float>(0,0)) + (_halfWidth);
	float pixelY = (_fy * pointPers.at<float>(1,0)) + (_halfHeight);
	
	cv::Point2i result(0,0);
	
	result.x = (int) pixelX;
	result.y = (int) pixelY;
	
	return result;
}

cv::Point3f VirtualCamera::inverseTransform(cv::Point2i pixel)
{
	//Begin inverse transform.
	//Inverse pixel transform (pixel coords to pointpers)
	float pointPersX = (((float)pixel.x) - _halfWidth) / _fx;
	float pointPersY = (((float)pixel.y) - _halfHeight) / _fy;
	
	//In forward transform, z-coordinate of pointPers is always equal to _nearClipPlane and w-coordinate is always 1
	cv::Mat pointPers(4, 1, CV_32FC1);
	pointPers.at<float>(0,0) = pointPersX;
	pointPers.at<float>(1,0) = pointPersY;
	pointPers.at<float>(2,0) = _nearClipPlane;
	pointPers.at<float>(3,0) = 1.0f;
	
	//Inverse pers transform (pointpers to pointcam)
	
	//Well...in forward transformation, this is the exact step that projects a 3D point onto a 2D screen.
	//We can not revert this, as it is in this step in the forward transformation where the
	//depth information about the 3D point is lost. It means we cannot reproduce the w-coordinate that
	//was used to normalize the 3D-point. So instead, we just use a value of 1 for w, which means we
	//calculate the intersection of the projection with the image plane.
	//The final result will be this point on the image plane in world coordinates.
	//The result suggests that the original 3D point was somewhere on the line that goes through
	//the position of the camera and this point.
	
	//In other words... in this step we do nothing.
	
	//Inverse camera transform (pointcam to pointworld)
	//(pointpers is equal to pointcam)
	
	cv::Mat pointWorld(4, 1, CV_32FC1);
	pointWorld = _camToWorld * pointPers;
	
	//std::cout << "z is " << pointWorld.at<float>(3,0) << std::endl;
	
	cv::Point3f result(pointWorld.at<float>(0,0), pointWorld.at<float>(1,0), pointWorld.at<float>(2,0));
	
	return result;
}






















































