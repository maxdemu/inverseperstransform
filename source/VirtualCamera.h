#pragma once

#include <opencv2/opencv.hpp>

class VirtualCamera
{
public:
	/*
	*	Create a new virtual camera with given parameters
	*	All 3D points are given in world coordinates
	*/
	VirtualCamera(
		cv::Point3f position,
		cv::Point3f reference,
		cv::Point3f	up,
		float fovX,
		float fovY,
		int pixelWidth,
		int pixelHeight,
		float nearClipPlane
	);
	
	/*
	*	Apply perspective transform on given 3D point and return pixel coordinates
	*/
	cv::Point2i forwardTransform(cv::Point3f point);
	
	/*
	*	Returns a 3D world coordinate of where the line of sight of this particular
	*	pixel in this camera would intersect with the image plane
	*/
	cv::Point3f inverseTransform(cv::Point2i pixel);
	
private:
	cv::Point3f _position;
	cv::Point3f _reference;
	cv::Point3f	_up;
	float _fovX;
	float _fovY;
	int _pixelWidth;
	int _pixelHeight;
	float _nearClipPlane;
	cv::Mat _camToWorld;
	cv::Mat _worldToCam;
	float _fx;
	float _fy;
	float _halfHeight;
	float _halfWidth;
};
