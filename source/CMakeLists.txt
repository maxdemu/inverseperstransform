cmake_minimum_required (VERSION 3.16)

project (InversPersTransform LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_PREFIX_PATH
"$ENV{QTDIR}"
"$ENV{QTDIR}/Qt6"
)  

find_package(OpenCV REQUIRED)
find_package(Qt6 REQUIRED COMPONENTS Widgets)

qt_standard_project_setup()

include_directories(${OpenCV_INCLUDE_DIRS})

add_executable(	InversPersTransform 
				main.cpp
				mainwindow.cpp
				mainwindow.h
				mainwindow.ui
				testcvmattype.h
				VirtualCamera.cpp
				VirtualCamera.h
				)

target_link_libraries(InversPersTransform ${OpenCV_LIBS})
target_link_libraries(InversPersTransform Qt6::Widgets)
