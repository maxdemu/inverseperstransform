#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <fstream>
#include <iostream>
#include <QFileDialog>
#include <QMessageBox>
#include <opencv2/opencv.hpp>

#include "testcvmattype.h"
#include "VirtualCamera.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _inputSpecified = false;
    _outputSpecified = false;
    _inputDir = "none";
    _outputDir = "none";
    _points.clear();
    //std::cout << "Size of Point3D is " << sizeof(cv::Point3d) << std::endl;
}

void MainWindow::slotButtonSelectInputDir()
{
	QString dir = QFileDialog::getExistingDirectory
	(
		this,
		tr("Open Directory"),
		"",
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks
	);

	if(!dir.isEmpty())
	{
		ui->labelInputDir->setText(dir);
		_inputDir = dir.toStdString();
		ui->pushButtonReadAndCalc->setEnabled(true);
		_inputSpecified = true;
	}
	else
	{
		QMessageBox msgBox;
		msgBox.setText("No custom directory was selected.");
		msgBox.exec();
	}

}

void MainWindow::slotButtonReadAndCalc()
{
	std::string imagePath = _inputDir + "/Snapshot.png";
	std::string paramPath = _inputDir + "/params.txt";
	
	//Read image
	cv::Mat image = cv::imread(imagePath);
	
	MatType(image);
	
	if(image.empty())
	{
		QMessageBox msgBox;
		msgBox.setText("Snapshot.png could not be opened");
		msgBox.exec();
		return;
	}
		
	cv::imshow("Image", image);
	
	//Read params
	std::ifstream paramFile(paramPath);
	
	if(!paramFile.is_open())
	{
		QMessageBox msgBox;
		msgBox.setText("params.txt could not be opened");
		msgBox.exec();
		paramFile.close();
		return;
	}
	
	//Start reading
	std::vector<std::string> lines;
	std::string line;
	while(std::getline(paramFile, line))
	{
		lines.push_back(line);
		//std::cout << line << std::endl;
	}
	
	//Strings are now stored
	//Check if size is correct
	
	if(lines.size() != 14)
	{
		QMessageBox msgBox;
		msgBox.setText("params.txt doesnt seem to be right size");
		msgBox.exec();
		paramFile.close();
		return;
	}
	
	//Extract all parameters
	
	float posx = std::stof(lines.at(0));
	float posy = std::stof(lines.at(1));
	float posz = std::stof(lines.at(2));
	
	float refx = std::stof(lines.at(3));
	float refy = std::stof(lines.at(4));
	float refz = std::stof(lines.at(5));
	
	float upx = std::stof(lines.at(6));
	float upy = std::stof(lines.at(7));
	float upz = std::stof(lines.at(8));
	
	float fovX = std::stof(lines.at(10));
	float fovY = std::stof(lines.at(9));
	
	int width = std::stoi(lines.at(11));
	int height = std::stoi(lines.at(12));
	
	float nearClipPlane = std::stof(lines.at(13));
	
	cv::Point3f pos(posx, posy, posz);
	cv::Point3f ref(refx, refy, refz);
	cv::Point3f	up(upx, upy, upz);
	
	//Show params on GUI
	std::string posText = "(" + lines.at(0) + ", " + lines.at(1) + ", " + lines.at(2) + ")"; 
	std::string refText = "(" + lines.at(3) + ", " + lines.at(4) + ", " + lines.at(5) + ")"; 
	std::string upText = "(" + lines.at(6) + ", " + lines.at(7) + ", " + lines.at(8) + ")"; 
	
	ui->labelCamPos->setText(QString::fromStdString(posText));
	ui->labelCamRef->setText(QString::fromStdString(refText));
	ui->labelCamUp->setText(QString::fromStdString(upText));
	ui->labelFOVX->setText(QString::number(fovX));
	ui->labelFOVY->setText(QString::number(fovY));
	ui->labelWidth->setText(QString::number(width));
	ui->labelHeight->setText(QString::number(height));
	ui->labelNearClipPlane->setText(QString::number(nearClipPlane));
	
	_points.clear();
	
	//Begin inverse transform
	//Define camera
	
	VirtualCamera cam(
		pos,
		ref,
		up,
		fovX,
		fovY,
		width,
		height,
		nearClipPlane
	);
	
	//Iterate over every pixel
	int rows = image.rows;
	int cols = image.cols;
	
	//int counter = 0;
	
	for(int row = 0; row < rows; row++)
	{
		for(int col = 0; col < cols; col++)
		{
			cv::Vec3b colorVec = image.at<cv::Vec3b>(row, col);
			cv::Point3f color(colorVec[0], colorVec[1], colorVec[2]);
			
			if(colorVec[0] == 83 && colorVec[1] == 83 && colorVec[2] == 83)
			{
			//	std::cout << "Pixel at (" << row << "," << col << ") is: (" << ((int)color[2]) << "," << ((int)color[1]) << "," << ((int)color[0]) << ")" << std::endl;
			//	counter++;
				continue;
			}
			
			//Now we have color and pixel coordinates and all other needed parameters for inverse transform.
			
			int pixelX = col;
			//OpenCV origin alignment; lower left upward to upper left downward
			int pixelY = rows - row;
			
			cv::Point2i pixelCoords(pixelX, pixelY);
			cv::Point3f worldPoint = cam.inverseTransform(pixelCoords);
			
			_points.push_back(worldPoint);
			_points.push_back(color);
			
		}
	}
	
	//int totalPixels = rows*cols;
	//int nonVoidPixels = counter;
	//float percentage = (((float)nonVoidPixels) / (float(totalPixels))) * 100.0f;
	//std::cout << nonVoidPixels << " of " << totalPixels << " pixels (" << percentage << "%) where non-void pixels" << std::endl;
	
	
	//Add dummy points
	//_points.push_back(cv::Point3d(-1,1,10.5));
	//_points.push_back(cv::Point3d(0,255,0));
	//_points.push_back(cv::Point3d(1,1,10.5));
	//_points.push_back(cv::Point3d(255,0,0));
	//_points.push_back(cv::Point3d(-1,-1,10.5));
	//_points.push_back(cv::Point3d(0,0,255));
	//_points.push_back(cv::Point3d(1,-1,10.5));
	//_points.push_back(cv::Point3d(255,255,0));
	
	//Set number of generated lines from inverse transform
	ui->labelLinesGenerated->setText(QString::number(_points.size()));
}

void MainWindow::slotButtonSelectOutputDir()
{
	QString dir = QFileDialog::getExistingDirectory
	(
		this,
		tr("Open Directory"),
		"",
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks
	);

	if(!dir.isEmpty())
	{
		ui->labelOutputDir->setText(dir);
		_outputDir = dir.toStdString();
		ui->pushButtonWrite->setEnabled(true);
		_outputSpecified = true;
	}
	else
	{
		QMessageBox msgBox;
		msgBox.setText("No custom directory was selected.");
		msgBox.exec();
	}
}

void MainWindow::slotButtonWrite()
{
	std::string outputPath = _outputDir + "/inverseLines.txt";
	std::ofstream output(outputPath);
	
	for(int i = 0; i < _points.size(); i++)
	{
		cv::Point3f p = _points.at(i);
		
		output << p.x << std::endl;
		output << p.y << std::endl;
		output << p.z << std::endl;
	}
	
	std::cout << "Done writing" << std::endl;
}

MainWindow::~MainWindow()
{
    delete ui;
}

